package cycles

import (
	"github.com/tompaton/goparsec"
	"strings"
	"tompaton/tbot/common/parser"
	"tompaton/tbot/common/when"
)

var keywords = parsec.TokenCI("/?(lists?|todo|done|tick|next|skip)", "KEYWORD")

func buildWhen() parsec.Parser {
	patterns := []string{}
	names := []string{}
	for _, pattern := range when.FutureDowKeywords() {
		patterns = append(patterns, pattern + "\\b")
		names = append(names, "WHEN")
	}

	return parsec.OrdTokensCI(patterns, names)
}
var futureWhen = buildWhen()

var word = parsec.Token("\\w+", "WORD")
var name = parsec.ManyUntil(nameNode, word, parsec.OrdChoice(parser.GetNode0, keywords, futureWhen))

var listsCommand = parsec.Maybe(listsNode, parsec.TokenCI("/?lists", "LISTS"))

var listCommand = parsec.And(
	listNode,
	parsec.TokenCI("/?list", "LIST"),
	name)

var categories = parsec.OrdTokensCI(
	[]string{"(show|with) details", "details", "(show|with) summary", "summary"},
	[]string{"DETAILS", "DETAILS", "SUMMARY", "SUMMARY"})

var todoCommand = parsec.And(
	setCategories,
	parsec.Maybe(
		todoNode,
		parsec.OrdTokensCI(
			[]string{"/?todo", "/?done"},
			[]string{"TODO", "DONE"})),
	parsec.Optional(
		parser.GetNode0,
		parsec.Many(nil, parsec.OrdChoice(parser.GetNode0, categories, futureWhen)),
		[]parsec.ParsecNode{}))

var cycleCommand = parsec.And(
	cycleNode,
	parsec.OrdTokensCI(
		[]string{"/?tick", "/?(next|skip)"},
		[]string{"TICK", "SKIP"}),
	name,
	parsec.Optional(parser.GetNode0, futureWhen, parsec.Terminal{"WHEN", "today", 0}))

var commands = parsec.OrdChoice(
	parser.GetNode0,
	listsCommand,
	listCommand,
	todoCommand,
	cycleCommand)

var Message = parsec.Many(nil, commands)

func listsNode(ns []parsec.ParsecNode) parsec.ParsecNode {
	if len(ns) == 0 {
		return nil
	}
	return newLists()
}

func listNode(ns []parsec.ParsecNode) parsec.ParsecNode {
	if len(ns) == 0 {
		return nil
	}
	return newList(ns[1].(string))
}

func todoNode(ns []parsec.ParsecNode) parsec.ParsecNode {
	if len(ns) == 0 {
		return nil
	}
	switch n := ns[0].(*parsec.Terminal).Name; n {
	case "TODO":
		return newTodo()
	case "DONE":
		return newDone()
	default:
		return nil
	}
}

func cycleNode(ns []parsec.ParsecNode) parsec.ParsecNode {
	if len(ns) == 0 {
		return nil
	}
	var name, when string
	name = ns[1].(string)
	switch w := ns[2].(type) {
	case parsec.Terminal:
		when = strings.ToLower(w.Value)
	case *parsec.Terminal:
		when = strings.ToLower(w.Value)
	}
	switch n := ns[0].(*parsec.Terminal).Name; n {
	case "TICK":
		return newTick(name, when)
	case "SKIP":
		return newSkip(name, when)
	default:
		return nil
	}
}

func setCategories(ns []parsec.ParsecNode) parsec.ParsecNode {
	if len(ns) != 0 {
		switch r := ns[0].(type) {
		case Todo:
			when := getCategory(ns[1].([]parsec.ParsecNode), "WHEN", r.When)
			show := getShow(ns[1].([]parsec.ParsecNode), r.Show)
			return Todo{show, when}
		case Done:
			when := getCategory(ns[1].([]parsec.ParsecNode), "WHEN", r.When)
			show := getShow(ns[1].([]parsec.ParsecNode), r.Show)
			return Done{show, when}
		}
		return ns[0]
	}
	return nil
}

func getCategory(ns []parsec.ParsecNode, terminal string, mode string) string {
	for _, n := range ns {
		if n.(*parsec.Terminal).Name == terminal {
			return strings.ToLower(n.(*parsec.Terminal).Value)
		}
	}
	return mode
}

func getShow(ns []parsec.ParsecNode, show string) string {
	for _, n := range ns {
		switch n.(*parsec.Terminal).Name {
		case "SUMMARY":
			return "summary"
		case "DETAILS":
			return "details"
		}
	}
	return show
}

func nameNode(ns []parsec.ParsecNode) parsec.ParsecNode {
	name := []string{}
	for _, n := range ns {
		name = append(name, n.(*parsec.Terminal).Value)
	}
	return strings.Join(name, " ")
}
