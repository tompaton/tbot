package cycles

import "testing"
import "tompaton/tbot/common/parser"

func TestParse(t *testing.T) {
	cases := []struct {
		in   string
		want []interface{}
	}{
		{"lists", []interface{}{Lists{}}},
                {"todo", []interface{}{Todo{"summary", "today"}}},
                {"todo summary", []interface{}{Todo{"summary", "today"}}},
                {"todo show details", []interface{}{Todo{"details", "today"}}},
                {"todo with details", []interface{}{Todo{"details", "today"}}},
                {"todo details", []interface{}{Todo{"details", "today"}}},
                {"done", []interface{}{Done{"summary", "today"}}},
                {"done summary", []interface{}{Done{"summary", "today"}}},
                {"done show details", []interface{}{Done{"details", "today"}}},
                {"done with details", []interface{}{Done{"details", "today"}}},
                {"done details", []interface{}{Done{"details", "today"}}},
                {"/list house", []interface{}{List{"house"}}},
                {" /tick run  /tick cold shower ", []interface{}{Tick{"run", "today"}, Tick{"cold shower", "today"}}},
                {"tick run tick cold shower", []interface{}{Tick{"run", "today"}, Tick{"cold shower", "today"}}},
                {"tick vacuum", []interface{}{Tick{"vacuum", "today"}}},
                {"Tick vacuum", []interface{}{Tick{"vacuum", "today"}}},
                {"Tick mop floors", []interface{}{Tick{"mop floors", "today"}}},  // m - monday, f - friday
                {"Tick mop", []interface{}{Tick{"mop", "today"}}},
                {"Tick floors", []interface{}{Tick{"floors", "today"}}},
		{"Tick water potplants", []interface{}{Tick{"water potplants", "today"}}},  // w - wednesday
                {"next vacuum friday", []interface{}{Skip{"vacuum", "friday"}}},
                {"tick camera photos todo monday", []interface{}{Tick{"camera photos", "today"}, Todo{"summary", "monday"}}},
                {"/tick camera photos", []interface{}{Tick{"camera photos", "today"}}},
                {"/skip vacuum friday", []interface{}{Skip{"vacuum", "friday"}}},
                {"tick sheets tick towels", []interface{}{Tick{"sheets", "today"}, Tick{"towels", "today"}}},
                {"tick lawn next lawn sunday todo", []interface{}{Tick{"lawn", "today"}, Skip{"lawn", "sunday"}, Todo{"summary", "today"}}},
	}

	for _, c := range cases {
		got := parser.Parse(Message, c.in)

		if got == nil {
			t.Errorf("Parse(%q) -> %q, wanted %q", c.in, got, nil)
		} else if len(got) != len(c.want) {
			t.Errorf("Parse(%q) -> %d commands, wanted %d, %q", c.in, len(got), len(c.want), got)
		} else {
			for i, g := range got {
				if g != c.want[i] {
					t.Errorf("Parse(%q):%d == %q, want %q", c.in, i, g, c.want[i])
				}
			}
		}
	}
}

func TestNotParsed(t *testing.T) {
	cases := []string{"", "tick tomorrow"}

	for _, c := range cases {
		got := parser.Parse(Message, c)
		if got != nil {
			t.Errorf("Parse(%q) == %q, want %q", c, got, nil)
		}
	}
}
