package cycles

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/cookiejar"
	"os"
	"strings"
	"time"
	"tompaton/tbot/common/when"
)

var _CYCLES = "http://cycles.tompaton.com/"
var _CYCLES_COOKIES, _ = cookiejar.New(nil)
var _CYCLES_HTTP = &http.Client{Jar: _CYCLES_COOKIES, Timeout: 10 * time.Second}
var _CYCLES_LOGGED_IN = false

func login() error {
	if !_CYCLES_LOGGED_IN {
	    print("Logging into Cycles...")

	    url := fmt.Sprintf("%slogin/%s", _CYCLES, os.Getenv("CYCLES_TOKEN"))
	    _, err := _CYCLES_HTTP.Get(url)

	    if err != nil {
		    print("Error.\n")
		    return err
	    } else {
		    _CYCLES_LOGGED_IN = true
		    print("Done.\n")
	    }
	}
	return nil
}

func get_cycles(url string, target interface{}) error {
	err := login()
	if err != nil {
		return err
	}
	res, err := _CYCLES_HTTP.Get(fmt.Sprintf("%sapi/%s/", _CYCLES, url))
	if err != nil {
		return err
	}
	defer res.Body.Close()
	return json.NewDecoder(res.Body).Decode(target)
}

func post_cycles(url string, data interface{}, target interface{}) error {
	err := login()
	if err != nil {
		return err
	}
	jsonData, _ := json.Marshal(data)
	res, err := _CYCLES_HTTP.Post(
		fmt.Sprintf("%sapi/%s/", _CYCLES, url),
		"application/json",
		bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}
	defer res.Body.Close()
	return json.NewDecoder(res.Body).Decode(target)
}

type jsonLists struct {
	Lists []struct{
		Title string
		Slug string
	}
}

func (v Lists) Handle() string {
	cycles := jsonLists{}
	get_cycles("list", &cycles)

	lists := []string{}
	for _, list := range cycles.Lists {
		lists = append(lists, fmt.Sprintf("%s: /list %s", list.Title, list.Slug))
	}
	return fmt.Sprintf("lists:\n%s", strings.Join(lists, "\n"))
}

type jsonList struct {
	Cycles []struct{
		Title string
		Slug string
		List_title string
		List_slug string
	}
}

func (v List) Handle() string {
	cycles := jsonList{}
	get_cycles(fmt.Sprintf("list/%s/cycle", v.Name), &cycles)

	list := []string{}
	for _, cycle := range cycles.Cycles {
		list = append(list,
			fmt.Sprintf("%s - %s: %s/%s",
				cycle.Title, cycle.List_title,
				cycle.Slug, cycle.List_slug))
	}
	return fmt.Sprintf("list:\n%s", strings.Join(list, "\n"))
}

type jsonCycle struct {
	Slug string
	Title string
	List_title string
	Summary string
	Lengths struct {
		Upcoming int
		Current int
	}
	Paused bool
}

type jsonCycles struct {
	Cycles []jsonCycle
}

func (v Todo) Handle() string {
	// TODO: support when kwarg
	cycles := select_cycles(due_today)
	return fmt.Sprintf(
		"To-do today:\n%s",
		show_cycles(cycles, v.Show))
}

func (v Done) Handle() string {
	// TODO: support when kwarg
	cycles := select_cycles(ticked_today)
	return fmt.Sprintf(
		"Ticked today:\n%s",
		show_cycles(cycles, v.Show))
}

func select_cycles(include func(jsonCycle) bool) jsonCycles {
	all_cycles := jsonCycles{}
	get_cycles("cycle", &all_cycles)

	selected := jsonCycles{}
	for _, cycle := range all_cycles.Cycles {
		if include(cycle) {
			selected.Cycles = append(selected.Cycles, cycle)
		}
	}

	return selected
}

func show_cycles(cycles jsonCycles, show string) string {
	list := []string{}

	for _, cycle := range cycles.Cycles {
		if show == "details" {
			list = append(list,
				fmt.Sprintf("%s - %s: %s",
					cycle.Title,
					cycle.List_title,
					cycle.Summary))
		}
		if show == "summary" {
			list = append(list, cycle.Title)
		}
	}

	return strings.Join(list, "\n")
}

func due_today(cycle jsonCycle) bool {
	return cycle.Lengths.Upcoming == 0 && !cycle.Paused
}

func ticked_today(cycle jsonCycle) bool {
	return cycle.Lengths.Current == 0
}

type jsonAction struct {
	Action string
}

func (v Tick) Handle() string {
	cycle, err := get_cycle(v.Name)
	if err != nil {
		return err.Error()
	}

	tWhen := when.GetPastDate(time.Now(), v.When)

	action := jsonAction{}
	post_cycles(
		fmt.Sprintf("cycle/%s/ticks", cycle.Slug),
		map[string]string{"time": tWhen.Format("2006-01-02T15:04:05")},
		&action)

/*
        // TODO: undo
        def _undo():
            action = post_cycles('cycle/{}/ticks'.format(cycle['slug']),
                                 time=when.isoformat()[:19])
            return "Un-ticked {title} - {list_title}, {action}".format(
                action=action['action'], **cycle)
*/

	return fmt.Sprintf(
		"Ticked %s - %s, %s",
		cycle.Title, cycle.List_title, action.Action)
}

func (v Skip) Handle() string {
	cycle, err := get_cycle(v.Name)
	if err != nil {
		return err.Error()
	}

	tWhen := when.GetFutureDate(time.Now(), v.When)

	action := jsonAction{}
	post_cycles(
		fmt.Sprintf("cycle/%s/schedule", cycle.Slug),
		map[string]string{"time": tWhen.Format("2006-01-02T15:04:05")},
		&action)

/*
        // TODO: undo
        def _undo():
            action = post_cycles('cycle/{}/schedule'.format(cycle['slug']),
                                 time=when.isoformat()[:19])
            return "Un-scheduled {title} - {list_title}, {action}".format(
                action=action['action'], **cycle)
*/

	return fmt.Sprintf(
		"Scheduled %s - %s, %s",
		cycle.Title, cycle.List_title, action.Action)
}

type CycleNotFound struct {
	msg string
}

func (e *CycleNotFound) Error() string {
	return e.msg
}

func get_cycle(name string) (jsonCycle, error) {
	all_cycles := jsonCycles{}
	get_cycles("cycle", &all_cycles)

	words := strings.Split(strings.ToLower(name), " ")

	cycles := []jsonCycle{}

	for _, cycle := range all_cycles.Cycles {
		if matches(cycle, words) {
			cycles = append(cycles, cycle)
		}
	}

	switch len(cycles) {
	case 0:
		return jsonCycle{}, &CycleNotFound{"Sorry, can't see which cycle you mean."}
	case 1:
		return cycles[0], nil
	default:
		titles := []string{}
		for _, cycle := range cycles {
			titles = append(titles,
				fmt.Sprintf(
					"%s - %s",
					cycle.Title, cycle.List_title))
		}
		return jsonCycle{}, &CycleNotFound{
			fmt.Sprintf(
				"Sorry, can't see which cycle you mean, it could be:\n%s",
				strings.Join(titles, "\n"))}
	}
}

func matches(cycle jsonCycle, words []string) bool {
	title := strings.Split(strings.ToLower(cycle.Title), " ")
	NextWord:
	for _, word := range words {
		for _, kw := range title {
			if strings.HasPrefix(kw, word) {
				continue NextWord
			}
		}
		// word didn't match any part of title
		return false
	}
	// all words matched
	return true
}
