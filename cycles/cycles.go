package cycles

import "fmt"

type Lists struct {
}

func newLists() Lists {
	return Lists{}
}

func (r Lists) String() string {
	return fmt.Sprintf("<Lists>")
}

type List struct {
	Name string
}

func newList(name string) List {
	return List{name}
}

func (r List) String() string {
	return fmt.Sprintf("<List name=%s>", r.Name)
}

type Todo struct {
	Show string
	When string
}

func newTodo() Todo {
	return Todo{"summary", "today"}
}

func (r Todo) String() string {
	return fmt.Sprintf("<Todo show=%s when=%s>", r.Show, r.When)
}

type Done struct {
	Show string
	When string
}

func newDone() Done {
	return Done{"summary", "today"}
}

func (r Done) String() string {
	return fmt.Sprintf("<Done show=%s when=%s>", r.Show, r.When)
}

type Tick struct {
	Name string
	When string
}

func newTick(name string, when string) Tick {
	return Tick{name, when}
}

func (r Tick) String() string {
	return fmt.Sprintf("<Tick name=%s when=%s>", r.Name, r.When)
}

type Skip struct {
	Name string
	When string
}

func newSkip(name string, when string) Skip {
	return Skip{name, when}
}

func (r Skip) String() string {
	return fmt.Sprintf("<Skip name=%s when=%s>", r.Name, r.When)
}
