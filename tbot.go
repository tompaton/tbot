package main

import (
	"github.com/tompaton/goparsec"
	"gopkg.in/telegram-bot-api.v4"
	"log"
	"os"
	"tompaton/tbot/common/parser"
	"tompaton/tbot/cycles"
	"tompaton/tbot/rain"
)

type CommandHandler interface {
	Handle() string
}

var parsers = []parsec.Parser{
	rain.Message,
	cycles.Message,
}

func main() {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TOKEN"))
	if err != nil {
		log.Panic(err)
	}

	// bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		for _, message := range parsers {
			commands := parser.Parse(message, update.Message.Text)
			if commands != nil {
				for _, command := range commands {
					reply := command.(CommandHandler).Handle()
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, reply)
					bot.Send(msg)
				}
				break
			}
		}
	}
}
