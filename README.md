# Tbot #

Experimental port of my telegram bot to Go.

Only implements commands to interact with http://rainfallrecord.info/ and http://cycles.tompaton.com/ at this point.

Should be able to get up and going by creating an `bot.sh` based on `bot.sh.example` with your API keys and tokens, then run:

```
#!bash
go install tompaton/tbot
./src/tompaton/tbot/bot.sh
```

Run tests with `go test tompaton/tbot`.