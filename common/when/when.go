package when

import "time"

var _DAYS = [][]string{
	// 'm' used for meat in lunch
	{"mon", "monday"},
	{"tu", "tue", "tuesday"},
	{"w", "wed", "wednesday"},
	{"th", "thu", "thursday"},
	{"f", "fri", "friday"},
	{"sa", "sat", "saturday"},
	{"su", "sun", "sunday"},
}

type dow_delta func(time.Weekday) int

func past(to_dow int) dow_delta {
	return func(dow time.Weekday) int {
		from_dow := int(dow) - 1
		return ((from_dow-to_dow)%7 + 7) % 7
	}
}

func future(to_dow int) dow_delta {
	return func(dow time.Weekday) int {
		from_dow := int(dow) - 1
		return ((to_dow-from_dow-1)%7+7)%7 + 1
	}
}

func offset(delta int) dow_delta {
	return func(from_dow time.Weekday) int { return delta }
}

var _PAST = map[string]dow_delta{}
var _FUTURE = map[string]dow_delta{}

func init() {
	for i, kws := range _DAYS {
		for _, kw := range kws {
			_PAST[kw] = past(i)
			_FUTURE[kw] = future(i)
		}
	}

	_PAST["today"] = offset(0)
	_PAST["yesterday"] = offset(1)

	_FUTURE["tomorrow"] = offset(1)
	_FUTURE["today"] = offset(0)
	_FUTURE["yesterday"] = offset(-1)
	_FUTURE["week"] = offset(7)
}

func PastDowKeywords() (keys []string) {
	for key := range _PAST {
		keys = append(keys, key)
	}
	return
}

func FutureDowKeywords() (keys []string) {
	for key := range _FUTURE {
		keys = append(keys, key)
	}
	return
}

func GetPastDate(now time.Time, dow string) time.Time {
	offset := _PAST[dow](now.Weekday())
	return now.Add(time.Duration(-1*24*offset) * time.Hour)
}

func GetFutureDate(now time.Time, dow string) time.Time {
	offset := _FUTURE[dow](now.Weekday())
	return now.Add(time.Duration(24*offset) * time.Hour)
}
