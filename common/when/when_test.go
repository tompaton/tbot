package when

import "testing"
import "time"

// run tests using same date
var now = time.Date(2017, time.April, 24, 9, 0, 0, 0, time.Local)

func TestPast(t *testing.T) {
	cases := []struct {
		kw      string
		offsets [7]int
		date    string
	}{
		{"today", [7]int{0, 0, 0, 0, 0, 0, 0}, "2017-04-24"},
		{"yesterday", [7]int{1, 1, 1, 1, 1, 1, 1}, "2017-04-23"},

		{"sunday", [7]int{0, 1, 2, 3, 4, 5, 6}, "2017-04-23"},
		{"sun", [7]int{0, 1, 2, 3, 4, 5, 6}, "2017-04-23"},
		{"su", [7]int{0, 1, 2, 3, 4, 5, 6}, "2017-04-23"},
		{"monday", [7]int{6, 0, 1, 2, 3, 4, 5}, "2017-04-24"},
		{"mon", [7]int{6, 0, 1, 2, 3, 4, 5}, "2017-04-24"},
		{"tuesday", [7]int{5, 6, 0, 1, 2, 3, 4}, "2017-04-18"},
		{"tue", [7]int{5, 6, 0, 1, 2, 3, 4}, "2017-04-18"},
		{"tu", [7]int{5, 6, 0, 1, 2, 3, 4}, "2017-04-18"},
		{"wednesday", [7]int{4, 5, 6, 0, 1, 2, 3}, "2017-04-19"},
		{"wed", [7]int{4, 5, 6, 0, 1, 2, 3}, "2017-04-19"},
		{"w", [7]int{4, 5, 6, 0, 1, 2, 3}, "2017-04-19"},
		{"thursday", [7]int{3, 4, 5, 6, 0, 1, 2}, "2017-04-20"},
		{"thu", [7]int{3, 4, 5, 6, 0, 1, 2}, "2017-04-20"},
		{"th", [7]int{3, 4, 5, 6, 0, 1, 2}, "2017-04-20"},
		{"friday", [7]int{2, 3, 4, 5, 6, 0, 1}, "2017-04-21"},
		{"fri", [7]int{2, 3, 4, 5, 6, 0, 1}, "2017-04-21"},
		{"f", [7]int{2, 3, 4, 5, 6, 0, 1}, "2017-04-21"},
		{"saturday", [7]int{1, 2, 3, 4, 5, 6, 0}, "2017-04-22"},
		{"sat", [7]int{1, 2, 3, 4, 5, 6, 0}, "2017-04-22"},
		{"sa", [7]int{1, 2, 3, 4, 5, 6, 0}, "2017-04-22"},
	}

	var offsets = [7]int{}
	for _, c := range cases {
		for i := 0; i < 7; i++ {
			offsets[i] = _PAST[c.kw](time.Weekday(i))
		}
		if offsets != c.offsets {
			t.Errorf("_PAST[%q] == %v, want %v", c.kw, offsets, c.offsets)
		}

		date := GetPastDate(now, c.kw).Format("2006-01-02")
		if date != c.date {
			t.Errorf("GetPastDate[%q] == %q, want %q", c.kw, date, c.date)
		}
	}
}

func TestFuture(t *testing.T) {
	cases := []struct {
		kw      string
		offsets [7]int
		date    string
	}{
		{"today", [7]int{0, 0, 0, 0, 0, 0, 0}, "2017-04-24"},
		{"yesterday", [7]int{-1, -1, -1, -1, -1, -1, -1}, "2017-04-23"},
		{"tomorrow", [7]int{1, 1, 1, 1, 1, 1, 1}, "2017-04-25"},
		{"week", [7]int{7, 7, 7, 7, 7, 7, 7}, "2017-05-01"},

		{"sunday", [7]int{7, 6, 5, 4, 3, 2, 1}, "2017-04-30"},
		{"sun", [7]int{7, 6, 5, 4, 3, 2, 1}, "2017-04-30"},
		{"su", [7]int{7, 6, 5, 4, 3, 2, 1}, "2017-04-30"},
		{"monday", [7]int{1, 7, 6, 5, 4, 3, 2}, "2017-05-01"},
		{"mon", [7]int{1, 7, 6, 5, 4, 3, 2}, "2017-05-01"},
		{"tuesday", [7]int{2, 1, 7, 6, 5, 4, 3}, "2017-04-25"},
		{"tue", [7]int{2, 1, 7, 6, 5, 4, 3}, "2017-04-25"},
		{"tu", [7]int{2, 1, 7, 6, 5, 4, 3}, "2017-04-25"},
		{"wednesday", [7]int{3, 2, 1, 7, 6, 5, 4}, "2017-04-26"},
		{"wed", [7]int{3, 2, 1, 7, 6, 5, 4}, "2017-04-26"},
		{"w", [7]int{3, 2, 1, 7, 6, 5, 4}, "2017-04-26"},
		{"thursday", [7]int{4, 3, 2, 1, 7, 6, 5}, "2017-04-27"},
		{"thu", [7]int{4, 3, 2, 1, 7, 6, 5}, "2017-04-27"},
		{"th", [7]int{4, 3, 2, 1, 7, 6, 5}, "2017-04-27"},
		{"friday", [7]int{5, 4, 3, 2, 1, 7, 6}, "2017-04-28"},
		{"fri", [7]int{5, 4, 3, 2, 1, 7, 6}, "2017-04-28"},
		{"f", [7]int{5, 4, 3, 2, 1, 7, 6}, "2017-04-28"},
		{"sa", [7]int{6, 5, 4, 3, 2, 1, 7}, "2017-04-29"},
		{"sat", [7]int{6, 5, 4, 3, 2, 1, 7}, "2017-04-29"},
		{"saturday", [7]int{6, 5, 4, 3, 2, 1, 7}, "2017-04-29"},
	}

	var offsets = [7]int{}
	for _, c := range cases {
		for i := 0; i < 7; i++ {
			offsets[i] = _FUTURE[c.kw](time.Weekday(i))
		}
		if offsets != c.offsets {
			t.Errorf("_FUTURE[%q] == %v, want %v", c.kw, offsets, c.offsets)
		}

		date := GetFutureDate(now, c.kw).Format("2006-01-02")
		if date != c.date {
			t.Errorf("GetFutureDate[%q] == %q, want %q", c.kw, date, c.date)
		}
	}
}
