package parser

import "github.com/tompaton/goparsec"

func Parse(message parsec.Parser, in string) (commands []interface{}) {
	s := parsec.NewScanner([]byte(in))
	vv, _ := message(s)
	if vv != nil {
		for _, v := range vv.([]parsec.ParsecNode)  {
			commands = append(commands, v)
		}
	}
	return
}

func GetNode0(ns []parsec.ParsecNode) parsec.ParsecNode {
	if len(ns) == 0 {
		return nil
	}
	return ns[0]
}
