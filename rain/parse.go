package rain

import (
	"github.com/tompaton/goparsec"
	"strconv"
	"strings"
	"tompaton/tbot/common/parser"
	"tompaton/tbot/common/when"
)

var rainKeyword = parsec.TokenCI("rain", "RAIN")
var of = parsec.Optional(nil, parsec.TokenCI("of", "OF"), parsec.Terminal{"OF", "of", 0})
var mm = parsec.Optional(nil, parsec.TokenCI("mm", "MM"), parsec.Terminal{"MM", "mm", 0})

var number = parsec.OrdChoice(toNumber, parsec.Float(), parsec.Int())
var amount = parsec.And(parser.GetNode0, number, mm)

func buildCategories() parsec.Parser {
	patterns := []string{"auto", "day", "month", "week"}
	names := []string{"NAME", "MODE", "MODE", "MODE"}
	for _, pattern := range when.PastDowKeywords() {
		patterns = append(patterns, pattern + "\\b")
		names = append(names, "WHEN")
	}

	return parsec.OrdTokensCI(patterns, names)
}
var categories = buildCategories()

var rainFirst = parsec.And(rainNode, rainKeyword, amount)
var rainLast = parsec.And(rainNode, amount, of, rainKeyword)

var rainCommand = parsec.And(
	setCategories,
	parsec.OrdChoice(parser.GetNode0, rainFirst, rainLast),
	// TODO: really shouldn't support repeating a single category,
	// permutation in python is better...
	parsec.Optional(parser.GetNode0, parsec.Many(nil, categories), []parsec.ParsecNode{}),
	parsec.End())

var commands = parsec.OrdChoice(
	parser.GetNode0,
	rainCommand)

var Message = parsec.Many(nil, commands)

func rainNode(ns []parsec.ParsecNode) parsec.ParsecNode {
	for _, n := range ns {
		switch n.(type) {
		case float64:
			return newRain(n.(float64))
		}
	}
	return nil
}

func setCategories(ns []parsec.ParsecNode) parsec.ParsecNode {
	if len(ns) != 0 {
		r := ns[0].(Rain)
		amount := r.Amount
		name := getCategory(ns[1].([]parsec.ParsecNode), "NAME", r.Name)
		mode := getCategory(ns[1].([]parsec.ParsecNode), "MODE", r.Mode)
		when := getCategory(ns[1].([]parsec.ParsecNode), "WHEN", r.When)
		return Rain{Amount: amount, Name: name, Mode: mode, When: when}
	}
	return nil
}

func getCategory(ns []parsec.ParsecNode, terminal string, mode string) string {
	for _, n := range ns {
		if n.(*parsec.Terminal).Name == terminal {
			return strings.ToLower(n.(*parsec.Terminal).Value)
		}
	}
	return mode
}

func toNumber(ns []parsec.ParsecNode) parsec.ParsecNode {
	term := ns[0].(*parsec.Terminal)
	val, _ := strconv.ParseFloat(term.Value, 64)
	return val
}
