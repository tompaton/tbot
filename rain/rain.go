package rain

import "fmt"

type Rain struct {
	Amount float64
	Name   string
	Mode   string
	When   string
}

func newRain(amount float64) Rain {
	return Rain{Amount: amount, Mode: "day", Name: "manual", When: "today"}
}

func (r Rain) String() string {
	return fmt.Sprintf("<Rain Amount=%0.1f Name=%s Mode=%s When=%s>",
		r.Amount, r.Name, r.Mode, r.When)
}
