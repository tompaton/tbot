package rain

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"
	"tompaton/tbot/common/when"
)

var _RAIN = "http://rainfallrecord.info/"

var _LOCATIONS = map[string]int {
	"auto": 572,
	"manual": 550,
}

func (record Rain) Handle() string {
	msg := ""
	location := _LOCATIONS[record.Name]
	tWhen := when.GetPastDate(time.Now(), record.When)
	amount := record.Amount

	if record.Mode == "month" {
		month, month_total := get_month_total(location, int(tWhen.Month()))
		amount -= month_total
		msg = fmt.Sprintf("Total for %s: %.1fmm\n", month, month_total)
	} else if record.Mode == "week" {
		start, end := get_week_dates(tWhen)
		week_total := get_week_total(location, start, end)
		amount -= week_total
		msg = fmt.Sprintf("Total for week: %.1fmm\n", week_total)
	}

	res, _ := http.PostForm(
		fmt.Sprintf("%slocations/%d/record.json", _RAIN, location),
		url.Values{
			"api_key": {os.Getenv("RAIN_TOKEN")},
			"record[date]": {tWhen.Format("2006-01-02")},
			"record[measurement]": {fmt.Sprintf("%.1f", amount)}})

	if res.StatusCode == http.StatusOK {
		return fmt.Sprintf("%sLogged %.1fmm of rain for %s (%s)",
			msg, amount, tWhen.Format("2006-01-02"), record.Name)
	} else {
		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)
		return string(body)
	}
}

type csvRow struct {
	Month string
	Amount [31]float64
}

func get_total(location int) [12]csvRow {
	var totals [12]csvRow

	totals_csv, _ := http.Get(fmt.Sprintf("%slocations/%d.csv", _RAIN, location))

	defer totals_csv.Body.Close()
	rows, _ := csv.NewReader(totals_csv.Body).ReadAll()
	for i, row := range rows[1:] {  // skip header
		totals[i].Month = row[0]
		for j, col := range row[1:] {
			amount, _ := strconv.ParseFloat(col, 64)
			totals[i].Amount[j] = amount
		}
	}

	return totals
}

func get_month_total(location int, month int) (string, float64) {
	total := 0.0
	totals := get_total(location)
	for _, amount := range totals[month - 1].Amount {
		total += amount
	}
	return totals[month - 1].Month, total
}

// NOTE: this won't work over the new year
func get_week_total(location int, start time.Time, end time.Time) float64 {
	total := 0.0
	totals := get_total(location)
	if start.Month() == end.Month() {
		for _, amount := range totals[start.Month() - 1].Amount[start.Day() - 1:end.Day()] {
			total += amount
		}
	} else {
		for _, amount := range totals[start.Month() - 1].Amount[start.Day() - 1:] {
			total += amount
		}
		for _, amount := range totals[end.Month() - 1].Amount[:end.Day()] {
			total += amount
		}
	}
	return total
}

func get_week_dates(tWhen time.Time) (time.Time, time.Time) {
	// Weekday returns Sunday=0, want Monday=0 like Python
	weekday := (int(tWhen.Weekday()) + 7) % 7
	start := tWhen.Add(time.Duration(-1 * 24 * weekday) * time.Hour)
	end := start.Add(time.Duration(24 * 6) * time.Hour)
	return start, end
}
