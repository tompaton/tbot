package rain

import "testing"
import "tompaton/tbot/common/parser"

func TestParse(t *testing.T) {
	cases := []struct {
		in   string
		want Rain
	}{
		{"rain 3", Rain{Amount: 3, Name: "manual", Mode: "day", When: "today"}},
		{"Rain 3", Rain{Amount: 3, Name: "manual", Mode: "day", When: "today"}},
		{"rain 0.3", Rain{Amount: 0.3, Name: "manual", Mode: "day", When: "today"}},
		{"rain .3", Rain{Amount: 0.3, Name: "manual", Mode: "day", When: "today"}},
		{"rain 3mm", Rain{Amount: 3, Name: "manual", Mode: "day", When: "today"}},
		{"rain 3.5mm", Rain{Amount: 3.5, Name: "manual", Mode: "day", When: "today"}},
		{"rain 3.5 mm", Rain{Amount: 3.5, Name: "manual", Mode: "day", When: "today"}},
		{"rain 3.5 Mm", Rain{Amount: 3.5, Name: "manual", Mode: "day", When: "today"}},
		{"3.5mm rain", Rain{Amount: 3.5, Name: "manual", Mode: "day", When: "today"}},
		{"3.5mm of rain", Rain{Amount: 3.5, Name: "manual", Mode: "day", When: "today"}},
		{"rain 3mm friday", Rain{Amount: 3, Name: "manual", Mode: "day", When: "friday"}},
		{"rain 3mm month", Rain{Amount: 3, Name: "manual", Mode: "month", When: "today"}},
		{"rain 3mm week", Rain{Amount: 3, Name: "manual", Mode: "week", When: "today"}},
		{"3.5mm rain auto", Rain{Amount: 3.5, Name: "auto", Mode: "day", When: "today"}},
		{"rain 3.5mm auto", Rain{Amount: 3.5, Name: "auto", Mode: "day", When: "today"}},
	}

	for _, c := range cases {
		commands := parser.Parse(Message, c.in)
		if commands == nil || len(commands) != 1 {
			t.Errorf("Parse(%q) == %q, want %q", c.in, commands, c.want)
		} else {
			got := commands[0].(Rain)
			if got != c.want {
				t.Errorf("Parse(%q) == %q, want %q", c.in, got, c.want)
			}
		}
	}
}

func TestNotParsed(t *testing.T) {
	cases := []string{"", "not rain", "rain 1.2d", "1.2d rain"}

	for _, c := range cases {
		got := parser.Parse(Message, c)
		if got != nil {
			t.Errorf("Parse(%q) == %q, want %q", c, got, nil)
		}
	}
}
